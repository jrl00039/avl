#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <set>
#include "VDinamico.h"
#include "VuelaFlight.h"
#include "Ruta.h"
#include "UTM.h"
#include "Aeropuerto.h"
#include "ListaEnlazada.h"
#include "Nodo.h"

using namespace std;

/**
 * Funcion para leer archivos CSV
 */
VDinamico<Aeropuerto> leerCSVAeropuertos() {
    // Vector a devolver con los datos
    VDinamico<Aeropuerto> vector;

    ifstream is;
    stringstream  columnas;
    string fila;

    // Variables para rellenar con la lectura
    string id;
    string iata;
    string tipo;
    string nombre;
    string latitud_str;
    string longitud_str;
    string continente;
    string iso_pais;
    float latitud, longitud;

    is.open("../aeropuertos_v2.csv");
    if ( is.good() ) {
        while ( getline(is, fila ) ) {
            if (!fila.empty()) {
                columnas.str(fila);

                getline(columnas, id, ';');
                getline(columnas,iata,';');
                getline(columnas,tipo,';');
                getline(columnas,nombre,';');
                getline(columnas,latitud_str,';');
                getline(columnas,longitud_str,';');
                getline(columnas,continente,';');
                getline(columnas,iso_pais,';');

                //  Transformamos la latitud y longitud a float
                latitud=stof(latitud_str);
                longitud=stof(longitud_str);

                // Limpiamos variables
                fila="";
                columnas.clear();

                // Creamos el objeto aeropuerto y lo insertamos en el VDinamico
                UTM posicion = UTM(latitud,longitud);
                Aeropuerto aeropuerto = Aeropuerto(id, iata, tipo, nombre, posicion, continente, iso_pais);
                vector.insertar(aeropuerto);
            }
        }
        is.close();
    } else {
        cout << "Error de apertura en archivo" << endl;
    }
    return vector;
}

ListaEnlazada<Ruta> leerCSVRutas(VDinamico<Aeropuerto> aeropuertos) {
    // Lista a devolver con los datos
    ListaEnlazada<Ruta> lista;

    ifstream is;
    stringstream  columnas;
    string fila;

    // Variables para rellenar con la lectura
    string aerolinea;
    string IATAorigen;
    string IATAdestino;

    is.open("../rutas_v1.csv");
    if ( is.good() ) {
        while ( getline(is, fila ) ) {
            if (!fila.empty()) {
                columnas.str(fila);

                getline(columnas, aerolinea, ';');
                getline(columnas, IATAorigen, ';');
                getline(columnas, IATAdestino, ';');

                // Limpiamos variables
                fila="";
                columnas.clear();

                // Creamos los aeropuertos con el IATA de IATAorigen y IATAdestino para encontrarlos en el VDinamico
                Aeropuerto buscarOrigen;
                buscarOrigen.setIata(IATAorigen);
                Aeropuerto buscarDestino;
                buscarDestino.setIata(IATAdestino);
                int indiceOrig = aeropuertos.busquedaBin(buscarOrigen);
                int indiceDest = aeropuertos.busquedaBin(buscarDestino);
                if (indiceOrig >= 0 && indiceDest >= 0) {
                    // Una vez encontrados, creamos la ruta
                    Aeropuerto origEncontrado = aeropuertos[indiceOrig];
                    Aeropuerto destEncontrado = aeropuertos[indiceDest];
                    Ruta ruta = Ruta(aerolinea, origEncontrado, destEncontrado);
                    lista.insertaFin(ruta);
                }
            }
        }
        is.close();
    } else {
        cout << "Error de apertura en archivo" << endl;
    }
    return lista;
}



/**
 * @author Jaime Rubio López jrl00039@red.ujaen.es
 * @author Dalton Gabriel Abambari Collaguazo dgac0002@red.ujaen.es
 */
int main(int argc, const char * argv[]) {
    // PROGRAMA DE PRUEBA 1
    //Crear un lista de enteros inicialmente vacía
    ListaEnlazada<int> lista;

    //Insertar al final de la lista los valores crecientes desde 101 a 200
    for (int i = 101; i <= 200; i++) {
        lista.insertaFin(i);
    }

    //Insertar por el comienzo de la lista los valores decrecientes desde 98 a 1
    for (int i = 98; i >= 1; i--) {
        lista.insertaInicio(i);
    }

    //Insertar el dato 100 delante del 101
    Iterador<int> it = lista.iterador();
    while (it.dato() != 101) {
        it.siguiente();
    }
    int valor = 100;
    lista.inserta(it, valor);

    //Insertar el dato 99 detrás del 98
    it = lista.iterador(); // Reiniciar el iterador al principio de la lista.
    while (it.dato() != 98) {
        it.siguiente();
    }
    valor = 99;
    lista.insertaDetras(it, valor);

    //Mostrar la lista resultante por pantalla
    it = lista.iterador(); // Reiniciar el iterador al principio de la lista.
    while (!it.fin()) {
        cout << it.dato() << " - ";
        it.siguiente();
    }
    cout << endl;

    //Borrar de la lista los 10 primeros y los 10 ultimos datos
    for (int i = 0; i < 10; ++i) {
        lista.borraInicio();
        lista.borraFinal();
    }

    //Borrar de la lista todos los multiplos de 10
    it = lista.iterador(); // Reiniciar el iterador al principio de la lista.
    while (!it.fin()) {
        if (it.dato() % 10 == 0) {
            lista.borra(it);
        } else {
            it.siguiente();
        }
    }

    //Mostrar la lista resultante por pantalla
    it = lista.iterador(); // Reiniciar el iterador al principio de la lista.
    while (!it.fin()) {
        cout << it.dato() << " - ";
        it.siguiente();
    }
    cout << endl;






    // PROGRAMA DE PRUEBA 2
    // Instanciar la clase VuelaFlight y rellenar el vector y la lista
    VDinamico<Aeropuerto> aeropuertos = leerCSVAeropuertos();
    aeropuertos.ordenar(); // Ordeno para poder hacer la busqueda de los IATAS al crear la lista enlazada
    ListaEnlazada<Ruta> rutas = leerCSVRutas(aeropuertos);
    VuelaFlight gestor = VuelaFlight(aeropuertos, rutas);

    // Buscar si hay ruta entre el aeropuerto de Barcelona (BCN) con el de Estambul (IST)
    const string bcn = "BCN";
    const string ist = "IST";
    Ruta* ruta = gestor.buscarRutasOriDes(bcn, ist);
    cout << "La primera ruta encontrada entre BCN e IST es de la aerolinea: " << ruta->getAerolinea() << endl;

    // Buscar también si hay ruta entre el aeropuerto de Granada-Jaén (GRX) con algún aeropuerto inglés (GB)
    const string inglaterra = "GB";
    ListaEnlazada<Ruta> rutasGranadaGB;
    VDinamico<Aeropuerto> aeropuertosIngleses = gestor.buscarAeropuertoPais(inglaterra);
    for (int i = 0; i < aeropuertosIngleses.tamLogico(); ++i) {
        Ruta* encontrada = gestor.buscarRutasOriDes(aeropuertosIngleses[i].getIata(), "GRX");
        Ruta* encontrada2 = gestor.buscarRutasOriDes("GRX", aeropuertosIngleses[i].getIata());
        if (encontrada != nullptr) rutasGranadaGB.insertaFin(*encontrada);
        if (encontrada2 != nullptr) rutasGranadaGB.insertaFin(*encontrada2);
    }
    cout << "Se han encontrado " << rutasGranadaGB.getNumElementos() << " rutas entre el aeropuerto GRX y alguno Ingles (GB). Son las siguiente" << endl;
    Nodo<Ruta> *buscador = rutasGranadaGB.getCabecera();
    for( int i=0; i<rutasGranadaGB.getNumElementos(); i++ ){
        cout << buscador->dato.getAerolinea() << ";" << buscador->dato.getOrigin().getIata() << ";" << buscador->dato.getDestination().getIata() << endl;
        buscador = buscador->sig;
    }

    // Añadir en O(1) una nueva ruta entre el aeropuerto de Granada-Jaén (GRX) con el de ParísCharles de Gaulle (CDG) de la compañía Iberia (IBE). Las rutas deben ir y volver entre ambos aeropuertos
    gestor.addNuevaRuta("GRX", "CDG", "IBE");

    // Buscar todas las rutas existentes entre España y Portugal. Medir los tiempos asociados
    /*clock_t ini = clock();
    ListaEnlazada<Ruta> rutasEspanaPortugal;
    ListaEnlazada<Ruta> rutasEspana;
    VDinamico<Aeropuerto> aeropuertosEspana = gestor.buscarAeropuertoPais("ES");
    VDinamico<Aeropuerto> aeropuertosPortugal = gestor.buscarAeropuertoPais("PT");
    aeropuertosPortugal.ordenar();
    for (int i=0; i<aeropuertosEspana.tamLogico(); i++){
        ListaEnlazada<Ruta> aux = gestor.buscarRutasOrigen(aeropuertosEspana[i].getIata());
        rutasEspana.concatena(aux);
    }
    Nodo<Ruta> *buscador2 = rutasEspana.getCabecera();
    for( int i=0; i<rutasEspana.getNumElementos(); i++ ){
        Aeropuerto aux;
        aux.setIata(buscador2->dato.getDestination().getIata());
        const int num = aeropuertosPortugal.busquedaBin(aux);
        if (num != -1) {
            rutasEspanaPortugal.insertaFin(buscador2->dato);
        }
        buscador2 = buscador2->sig;
    }
    cout << "Se han encontrado " << rutasEspanaPortugal.getNumElementos() << " rutas entre Espana y portugal. Son las siguiente" << endl;
    Nodo<Ruta> *mostrar = rutasEspana.getCabecera();
    for( int i=0; i<rutasEspana.getNumElementos(); i++ ){
        cout << mostrar->dato.getAerolinea() << ";" << mostrar->dato.getOrigin().getIata() << ";" << mostrar->dato.getDestination().getIata();
        mostrar = mostrar->sig;
    }

    cout << "Tiempo busqueda aeropuertos Espana-Portugal: " << ((clock() - ini) / (float) CLOCKS_PER_SEC) << " segs." << endl;*/

    return 0;
}