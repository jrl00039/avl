//
// Created by Dalton Gabriel Abambari on 18/10/23.
//

#ifndef ITERADOR_H
#define ITERADOR_H

#include "ListaEnlazada.h"
#include "Nodo.h"

template<class T>
class Iterador {
public:
    Nodo<T> *nodo;

    Iterador<T>(){
        nodo = NULL;
    }

    Iterador<T>(Nodo<T> *aNodo):nodo(aNodo){

    }

    bool fin(){
        return nodo == 0;
    }

    void siguiente(){
        nodo = nodo->sig;
    }

    void anterior(){
        nodo = nodo->ant;
    }

    T &dato(){
        return nodo->dato;
    }
};

#endif /* ITERADOR_H */