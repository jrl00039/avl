#include "VuelaFlight.h"

using namespace std;

VuelaFlight::VuelaFlight() = default;

VuelaFlight::VuelaFlight(const VDinamico<Aeropuerto>& airports, const ListaEnlazada<Ruta>& routes) {
    VuelaFlight::airports = airports;
    VuelaFlight::routes = routes;
}

VuelaFlight::~VuelaFlight() {
}

VDinamico<Aeropuerto> VuelaFlight::getAirports() const {
    return airports;
}

void VuelaFlight::setAirports(VDinamico<Aeropuerto> airports) {
    VuelaFlight::airports = airports;
}

ListaEnlazada<Ruta> VuelaFlight::getRoutes() const {
    return routes;
}

void VuelaFlight::setRoutes(ListaEnlazada<Ruta> routes) {
    VuelaFlight::routes = routes;
}


Ruta* VuelaFlight::buscarRutasOriDes(const string& originId, const string& destinationId) const {
    if( routes.getCabecera() == nullptr ) {
        cout << "No se ha podido encontrar la ruta porque la lista esta vacia";
    }else{
        Nodo<Ruta> *buscador = routes.getCabecera();
        for( int i=0; i<routes.getNumElementos(); i++ ){
            if( buscador->dato.getOrigin().getIata() == originId && buscador->dato.getDestination().getIata() == destinationId ){
                return &(buscador->dato);
            }
            buscador = buscador->sig;
        }
        return nullptr;
    }
}

ListaEnlazada<Ruta> VuelaFlight::buscarRutasOrigen(const string& originId) const {
    ListaEnlazada<Ruta> rutasEncontradas;
    if( routes.getCabecera() == nullptr ) {
        cout << "No se ha podido encontrar la ruta porque la lista esta vacia";
    }else{
        Nodo<Ruta> *buscador = routes.getCabecera();
        for( int i=0; i<routes.getNumElementos(); i++ ){
            if( buscador->dato.getOrigin().getIata() == originId){
                rutasEncontradas.insertaFin(buscador->dato);
            }
            buscador = buscador->sig;
        }
        return rutasEncontradas;
    }
}

VDinamico<Aeropuerto> VuelaFlight::buscarAeropuertoPais(const string& country) const {
    VDinamico<Aeropuerto> encontrados;
    for (int i=0; i<airports.tamLogico(); i++) {
        if (airports[i].getIsoPais() == country) {
            encontrados.insertar(airports[i]);
        }
    }
    return encontrados;
}

void VuelaFlight::addNuevaRuta(const string& originId, const string& destinationId, const string& airline) {
    Aeropuerto aeropuertoOrig;
    aeropuertoOrig.setIata(originId);
    Aeropuerto aeropuertoDest;
    aeropuertoDest.setIata(destinationId);
    int indiceOrig = airports.busquedaBin(aeropuertoOrig);
    int indiceDest = airports.busquedaBin(aeropuertoDest);
    if (indiceOrig != -1 && indiceDest != -1) {
        Ruta ruta = Ruta(airline, airports[indiceOrig], airports[indiceDest]);
        routes.insertaFin(ruta);
    } else cout << "No se pudo crear la ruta" << endl;
}