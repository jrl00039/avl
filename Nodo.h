//
// Created by Dalton Gabriel Abambari on 18/10/23.
//

#ifndef NODO_H
#define NODO_H
#include "Ruta.h"

template<class T>
class Nodo {
public:
    T dato;
    Nodo *sig;
    Nodo *ant;

    Nodo<T>(T& aDato, Nodo *aSig = 0, Nodo *aAnt = 0) {
        dato = aDato;
        sig = aSig;
        ant = aAnt;
    }


    T getSig() {
        this = this->sig;
        return this;
    }


    void setSig(unsigned a) {
        this->sig = a;

    }


    T getAnt() const {
        return ant;
    }


    void setAnt(unsigned a) {
        this->ant = a;
    }

};

#endif /* NODO_H */