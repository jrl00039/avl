//
// Created by Dalton Gabriel Abambari on 18/10/23.
//

#include "Ruta.h"

Ruta::Ruta()= default;

Ruta::Ruta(const string &aerolinea, const Aeropuerto& origin, const Aeropuerto& destination)
        : _aerolinea(aerolinea), _origin(origin), _destination(destination) {
    _aerolinea= aerolinea;
    _origin=origin;
    _destination=destination;
}


Ruta::Ruta(const Ruta &orig) {
    this->_aerolinea=orig._aerolinea;
    this->_origin=orig._origin;
    this->_destination=orig._destination;
}

Ruta::~Ruta() {

}

const string &Ruta::getAerolinea() const {
    return _aerolinea;
}
void Ruta::setAerolinea(const string &areolinea) {
    _aerolinea = areolinea;
}

Aeropuerto Ruta::getOrigin() const {
    return _origin;
}

void Ruta::setOrigin(const Aeropuerto& origin) {
    _origin = origin;
}

Aeropuerto Ruta::getDestination() const {
    return _destination;
}

void Ruta::setDestination(const Aeropuerto& destination) {
    _destination = destination;
}
