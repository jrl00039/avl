//
// Created by Dalton Gabriel Abambari on 18/10/23.
//

#ifndef LISTAENLAZADA_VUELAFLIGHT_H
#define LISTAENLAZADA_VUELAFLIGHT_H
#include <iostream>
#include <vector>
#include <list>
#include <string>
#include "Ruta.h"
#include "Aeropuerto.h"
#include "VDinamico.h"
#include "ListaEnlazada.h"

using namespace std;

class VuelaFlight {
public:
    VuelaFlight();
    VuelaFlight(const VDinamico<Aeropuerto>& airports, const ListaEnlazada<Ruta>& routes);
    virtual ~VuelaFlight();

    // Getters and Setters
    VDinamico<Aeropuerto> getAirports() const;
    void setAirports(VDinamico<Aeropuerto> airports);
    ListaEnlazada<Ruta> getRoutes() const;
    void setRoutes(ListaEnlazada<Ruta> routes);

    Ruta* buscarRutasOriDes(const string& originId, const string& destinationId) const;
    ListaEnlazada<Ruta> buscarRutasOrigen(const string& originId) const;
    VDinamico<Aeropuerto> buscarAeropuertoPais(const string& country) const;
    void addNuevaRuta(const string& originId, const string& destinationId, const string& airline);

private:
    VDinamico<Aeropuerto> airports;
    ListaEnlazada<Ruta> routes;
};
#endif //LISTAENLAZADA_VUELAFLIGHT_H
