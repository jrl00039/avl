//
// Created by Dalton Gabriel Abambari on 18/10/23.
//

#ifndef LISTAENLAZADA_H
#define LISTAENLAZADA_H

#include <iostream>
#include "Nodo.h"
#include "Iterador.h"

using namespace std;

template<class T>
class ListaEnlazada {
    private:
        Nodo<T> *cola, *cabecera;
        int numElementos;

    public:
        /**
         * Constructor por defecto
         */
        ListaEnlazada<T>() :cola(0), cabecera(0), numElementos(0) {}

        /**
         * Constructor copia
         * @param orig Elemento a copiar
         */
        ListaEnlazada<T>(const ListaEnlazada& orig) :cola(0), cabecera(0), numElementos(orig.numElementos) {
            if (orig.getCabecera() == 0) {
                throw range_error("La lista original está vacía");
            }
            cabecera = new Nodo<T>(orig.cabecera->dato);
            Nodo<T> *iterador2 = orig.cabecera;
            while (iterador2 != 0) {
                Nodo<T> *iterador = new Nodo<T>(iterador2->dato);
                iterador2->sig = iterador;
                iterador->ant = iterador2;
                cola = iterador;
                iterador2 = iterador2->sig;
            }
        }

        //DESTRUCTOR
        virtual ~ListaEnlazada<T>() {
            this->borratodo();
            if (this->cabecera == 0) {
                cout << "lista borrada correctamente";
            }

        }

        //SETTERS Y GETTERS DE LOS ATRIBUTOS
        Nodo<T>* getCabecera() const { return cabecera; }
        void setCabecera(Nodo<T>* cabecera) { this->cabecera = cabecera; }
        Nodo<T>* getCola() const { return cola; }
        void setCola(Nodo<T>* cola) { this->cola = cola; }
        int getNumElementos() const { return numElementos; }
        void setNumElementos(int numElementos) { this->numElementos = numElementos; }

        /**
         * Operador asignacion
         */
        ListaEnlazada<T> &operator=(const ListaEnlazada<T> &f){
            cola = f.cola;
            cabecera = f.cabecera;
            numElementos = f.numElementos;
            return *this;
        }

    public:
        pair<bool, Iterador<T>> buscar(const T &dato, Iterador<T> &it) const;
        T& inicio();
        T& fin();
        Iterador<T> iterador() const;
        void insertaInicio(T& dato);
        void insertaFin(T &dato);
        void inserta(Iterador<T>  &i, T &dato);
        void insertaDetras(Iterador<T>  &i, T &dato);
        void borraInicio();
        void borraFinal();
        void borra(Iterador<T> &i);
        int tam();
        void borratodo();
        //void concatena(const ListaEnlazada<T> &l);
        void borra(Iterador<T> &i, int num);
};






/**
 * DEVUELVE TRUE SI SE ENCUENTRA EL DATO EN LA LISTA
 * @tparam T
 * @param dato
 * @param it
 * @return
 */
template<class T>
pair<bool, Iterador<T>> ListaEnlazada<T>::buscar(const T &dato, Iterador<T> &it) const {
    pair<bool, Iterador<T>> ret; //VALORES A DEVOLVER
    if( cabecera == 0 ){
        cout << "No se ha podido encontrar porque la lista esta vacia";
    }else{
        Nodo<T> *buscador = cabecera;
        bool flag = false;
        for( int i=0; i<numElementos; i++ ){
            if( buscador->dato == dato ){
                flag = true;
                it = buscador;
                ret.first = flag;
                ret.second = it;
                return ret;
            }
            buscador = buscador->sig;
        }
        if( !flag ){ //SI NO SE HA ACTIVADO LA BANDERA, NO SE HA ENCONTRADO EL DATO ASIQUE SE DEVUELVE FALSE
            ret.first = flag;
            ret.second = 0;
            return ret;
        }
    }
}

/*//DEVUELVE EL ELEMENTO DE LA LISTA, SI SE HA ENCONTRADO
T buscar(T &dato){
    if( cabecera == 0 ){
        cout << "No se ha podido encontrar porque la lista esta vacia";
    }else{
        Nodo<T> *buscador = cabecera;
        bool flag = false;
        for( int i=0; i<numElementos; i++ ){
            if( buscador->dato == dato ){
                flag = true;
                return buscador->dato;
            }
            buscador = buscador->sig;
        }
        if( !flag ){ //SI NO SE HA ACTIVADO LA BANDERA, NO SE HA ENCONTRADO EL DATO ASIQUE SE DEVUELVE FALSE
            return dato;
        }
    }
}*/

//DEVUELVE DATO DE LA CABECERA
template<class T>
T& ListaEnlazada<T>::inicio() {
    if (cabecera == 0) {
        throw invalid_argument("La lista esta vacia");
    }
    return cabecera->dato;
}

//DEVUELVE DATO DE LA COLA
template<class T>
T& ListaEnlazada<T>::fin() {
    if (cola == 0) {
        throw invalid_argument("La lista esta vacia");
    }
    return cola->dato;
}

//DEVULEVE UN OBJETO ITERADOR PARA ITERAR SOBRE UNA LISTA
template<class T>
Iterador<T> ListaEnlazada<T>::iterador() const {
    Iterador<T> i(this->cabecera);
    return i;
}

//INSERTA AL PRINCIPIO DE LA LISTA
template<class T>
void ListaEnlazada<T>::insertaInicio(T& dato) {
    Nodo<T> *nuevo;
    nuevo = new Nodo<T>(dato,cabecera,0);
    if (cola == 0) {
        cola = nuevo;
    }
    if(cabecera != 0){
        cabecera->ant = nuevo;
    }
    cabecera = nuevo;
    numElementos++;
}

//INSERTA AL FINAL DE LA LISTA
template<class T>
void ListaEnlazada<T>::insertaFin(T &dato) {
    Nodo<T> *nuevo;
    nuevo = new Nodo<T>(dato,0,cola);
    if (cola != 0) {
        cola->sig = nuevo;
    }
    if (cabecera == 0) {
        cabecera = nuevo;
    }
    cola = nuevo;
    numElementos++;
}

//INSERTA UN DATO EN LA POSICION ANTERIOR APUNTADA POR UN ITERADOR
template<class T>
void ListaEnlazada<T>::inserta(Iterador<T>  &i, T &dato) {
    if (i.nodo == nullptr) throw invalid_argument("El iterador no apunta a ningún nodo.");
    Nodo<T> *anterior = 0;
    if (cabecera != cola) {
        anterior = cabecera;
        while (anterior->sig != i.nodo) {
            anterior = anterior->sig;
        }
    }
    Nodo<T> *nuevo;
    nuevo = new Nodo<T>(dato, i.nodo,anterior);
    anterior->sig = nuevo;
    if (cabecera == 0) {
        cabecera = cola = nuevo;
    }
    numElementos++;
}

template<class T>
void ListaEnlazada<T>::insertaDetras(Iterador<T>  &i, T &dato) {
    if (i.nodo == nullptr) throw invalid_argument("El iterador no apunta a ningún nodo.");
    Nodo<T> *siguiente = i.nodo->sig;
    Nodo<T> *nuevo = new Nodo<T>(dato, siguiente, i.nodo);
    i.nodo->sig = nuevo;
    if (siguiente != nullptr) {
        siguiente->ant = nuevo;
    } else {
        cola = nuevo; // Si estamos insertando al final de la lista, actualizamos la cola.
    }
    numElementos++;
}

//BORRA LA CABECERA
template<class T>
void ListaEnlazada<T>::borraInicio() {
    Nodo<T> *borrado = cabecera;
    cabecera = cabecera->sig;
    delete borrado;
    if (cabecera != 0) {
        cabecera->ant = 0;
    }else{
        cola = 0;
    }
    numElementos--;
}



//BORRA LA COLA
template<class T>
void ListaEnlazada<T>::borraFinal() {
    Nodo<T> *borrado = cola;
    cola = cola->ant;
    delete borrado;
    if(cola != 0){
        cola->sig = 0;
    }else{
        cabecera = 0;
    }
    numElementos--;
}

//BORRA ELEMENTO REFERENCIADO POR UN ITERADOR
template<class T>
void ListaEnlazada<T>::borra(Iterador<T> &i) {
    Nodo<T> *anterior = 0;
    Nodo<T> *aux;
    if (cabecera != cola) {
        anterior = cabecera;
        while (anterior->sig != i.nodo) {
            anterior = anterior->sig;
        }
        aux = anterior->sig;
        anterior->sig = aux->sig;
        if( aux->sig == nullptr ){
            cola = anterior;
        }
        i.siguiente();
        delete aux;
    }
    numElementos--;
}

//DEVUELVE NUMERO DE ELEMENTOS DE LA LISTA
template<class T>
int ListaEnlazada<T>::tam() {
    return numElementos;
}

//METODO QUE LLAMO DESDE EL DESTRUCTOR PARA BORRAR TODA LA LISTA
template<class T>
void ListaEnlazada<T>::borratodo() {
    Nodo<T> *aux = cabecera;
    Nodo<T> *aux2 = 0;
    while (aux != 0) {
        aux2 = aux;
        aux = aux->sig;
        delete aux2;
    }
    cabecera = cola = 0;
    numElementos = 0;
}

//CONCATENA UNA LISTA CON OTRA
/*template<class T>
void ListaEnlazada<T>::concatena(const ListaEnlazada<T> &l) {
    if (cola == 0 && cabecera == 0) {
        cola = Nodo<T>();
        cabecera = Nodo<T>();
    } else {
        cola->sig = l.getCabecera();
        l.getCabecera()->ant = cola;
        cola = l.cola;
    }
}*/

//BORRA "n" DATOS DESDE LA POSICION APUNTADA POR EL ITERADOR
template<class T>
void ListaEnlazada<T>::borra(Iterador<T> &i, int num) {
    if (num > numElementos) {
        throw out_of_range("Se quiere borrar algo que no existe");
    }
    if (i == 0) {
        throw range_error("El iterador no apunta a nada");
    }
    for (int j = 0; j < num; j++) {
        this->borra(i);
        i = i.siguiente();
    }
}

#endif /* LISTAENLAZADA_H */
