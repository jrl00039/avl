//
// Created by Dalton Gabriel Abambari on 18/10/23.
//

#ifndef LISTAENLAZADA_RUTA_H
#define LISTAENLAZADA_RUTA_H
#include <string>
#include "Aeropuerto.h"

using namespace std;

class Ruta {
private:
    string _aerolinea;
    Aeropuerto _origin;
    Aeropuerto _destination;

public:
    Ruta();
    Ruta(const string& aerolinea, const Aeropuerto& origin, const Aeropuerto& destination);
    Ruta(const Ruta& orig);
    virtual ~Ruta();

    const string& getAerolinea() const;
    void setAerolinea(const string& aerolinea);

    Aeropuerto getOrigin() const;
    void setOrigin(const Aeropuerto& origin);

    Aeropuerto getDestination() const;
    void setDestination(const Aeropuerto& destination);
};


#endif //LISTAENLAZADA_RUTA_H
